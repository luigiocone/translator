package translator;

import java.util.regex.Pattern;

public class Translator {
	
	private String sentence;
	private static final int flag = Pattern.CASE_INSENSITIVE;
	
	public Translator(String sentence) {
		this.sentence = sentence;
	}
	
	public String getSentence() {
		return sentence;
	}

	public String translate() throws PigLatinException {
		if (sentence.isEmpty()) return "nil";
		
		String regex = "[.,;:?!'()\\- ]";
		if (!Pattern.compile(regex, flag).matcher(sentence).find()) 
			return this.translateSingleWord(sentence);

		String[] words = sentence.split("(?<=" + regex + ")");
		String result = "";
		int currLen;
		for(String word : words) {
			currLen = word.length();
			if(currLen == 1) 
				result += word;
			else if(Pattern.compile("^" + regex + "$", flag).matcher(String.valueOf(word.charAt(currLen-1))).find()) 
				result += this.translateSingleWord(word.substring(0, currLen-1)) + word.charAt(currLen-1);
			else
				result += this.translateSingleWord(word);
		}
		return result;
	}
	
	public String translateSingleWord(String word) throws PigLatinException {
		boolean uppercase = false;
		String result = "";
		if(word.toUpperCase().equals(word)) 
			uppercase = true;
		if (!uppercase && !word.toLowerCase().equals(word))
			throw new PigLatinException("CamelCase not allowed");
		
		if (Pattern.compile("^[aeiou][a-z]*$", flag).matcher(word).find()) {
			if (Pattern.compile("y$", flag).matcher(word).find())
				result = word + "nay";
			else if (Pattern.compile("[aeiou]$", flag).matcher(word).find())
				result = word + "yay";
			else if (Pattern.compile("[a-z&&[^aeiou]]$", flag).matcher(word).find())
				result = word + "ay";
		}
		else if (Pattern.compile("^[a-z&&[^aeiou]][a-z]*$", flag).matcher(word).find()) {
			result = word;
			while(Pattern.compile("^[a-z&&[^aeiou]][a-z]*$", flag).matcher(result).find())
				result = result.substring(1, word.length()) + result.charAt(0);
			result += "ay";
		}
		else 
			throw new PigLatinException("Sentence is not well formatted: " + sentence);;
		
		if(uppercase) result = result.toUpperCase();
		return result;
	}

}

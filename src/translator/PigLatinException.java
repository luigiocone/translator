package translator;

public class PigLatinException extends RuntimeException {

	public PigLatinException() { }

	public PigLatinException(String message) {
		super(message);
	}

	public PigLatinException(Throwable cause) {
		super(cause);
	}

	public PigLatinException(String message, Throwable cause) {
		super(message, cause);
	}

	public PigLatinException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}

package translator;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

import org.junit.Test;

public class TestTranslator {

	@Test
	// Test User Story #1
	public void testGetSentence() {
		String sentence = "hello world";
		String observed = new Translator(sentence).getSentence(); 
		assertEquals(sentence, observed);
	}
	
	@Test
	// Test User Story #2
	public void testTranslateEmptySentence() {
		String sentence = "";
		String observed = new Translator(sentence).translate(); 
		assertEquals("nil", observed);
	}
	
	@Test
	// Test User Story #3
	public void testTranslateStartsVowelEndsY() {
		String sentence = "uigy";
		String observed = new Translator(sentence).translate(); 
		assertEquals(sentence+"nay", observed);
	}
	
	@Test
	// Test User Story #3
	public void testTranslateStartsVowelEndsVowel() {
		String sentence = "uigi";
		String observed = new Translator(sentence).translate(); 
		assertEquals(sentence+"yay", observed);
	}
	
	@Test
	// Test User Story #3
	public void testTranslateStartsVowelEndsConsonant() {
		String sentence = "uigil";
		String observed = new Translator(sentence).translate(); 
		assertEquals(sentence+"ay", observed);
	}
	
	@Test(expected = PigLatinException.class)
	// Test User Story #3
	public void testTranslateStartsVowelEndsAlphaNumeric() {
		String sentence = "uigi_";
		String observed = new Translator(sentence).translate(); 
	}
	
	@Test
	// Test User Story #4
	public void testTranslateStartsConsonant() {
		String sentence = "luigi";
		String observed = new Translator(sentence).translate();	
		String expected = sentence.substring(1, sentence.length()) + sentence.charAt(0) + "ay";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #5
	public void testTranslateStartsMultipleConsonant() {
		String sentence = "lluigi";
		String observed = new Translator(sentence).translate();	
		String expected = "uigillay";
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #6
	public void testTranslateMultipleWordsSeparatedBySpaces() {
		String sentence = "hello world";
		String observed = new Translator(sentence).translate();		
		String expected = "ellohay orldway";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #6
	public void testTranslateMultipleWordsSeparatedByDash() {
		String sentence = "well-being";
		String observed = new Translator(sentence).translate();		
		String expected = "ellway-eingbay";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #7
	public void testTranslateWithPuntuactions() {
		String sentence = "hello world!";
		String observed = new Translator(sentence).translate();	
		String expected = "ellohay orldway!";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #7
	public void testTranslateWithMultiplePuntuactions() {
		String sentence = "hello, world!";
		String observed = new Translator(sentence).translate();	
		String expected = "ellohay, orldway!";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #7
	public void testTranslateWordsSeparatedByDashAndSpacesWithPuntuactions() {
		String sentence = "hello brave-new-world!";
		String observed = new Translator(sentence).translate();	
		String expected = "ellohay avebray-ewnay-orldway!";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #8
	public void testTranslateUpperCase() {
		String sentence = "APPLE";
		String observed = new Translator(sentence).translate();	
		String expected = "APPLEYAY";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #8
	public void testTranslateMultipleWordsInUppercase() {
		String sentence = "APPLE AND PEAR";
		String observed = new Translator(sentence).translate();	
		String expected = "APPLEYAY ANDAY EARPAY";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test
	// Test User Story #8
	public void testTranslateUpperCaseWithPuntuactions() {
		String sentence = "APPLE!";
		String observed = new Translator(sentence).translate();	
		String expected = "APPLEYAY!";
		
		assertEquals("Expected: " + expected + "\nObserved: " + observed, expected, observed);
	}
	
	@Test(expected = PigLatinException.class)
	// Test User Story #8
	public void testTranslateCamelCase() {
		String sentence = "ApPlE";
		new Translator(sentence).translate();
	}

}
